package TimeComplexity;

import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Amin Khaki Moghadam on 01/11/2016.
 * Time Complexity Project - ArrayMaker
 */
class ArrayMaker {
    int i=0;
    int[] num = new int[i];
    void input(){
        System.out.println("Enter length: ");
        Scanner in = new Scanner(System.in);
        this.i = in.nextInt();
        int[] numTemp = new int[i]; //If I don't do this I have nullpointer error cause num can't initialize with (i) itself.
        for (int j=0; j<this.i; j++){
            numTemp[j]= ThreadLocalRandom.current().nextInt(0,100);
        }
        this.num = numTemp;
    }
    void sort(){
        Arrays.sort(this.num);
    }
}
